#
# Cookbook Name:: chef-dk
# Recipe:: default
#
# Copyright 2015, Aziz Ahmad Afandi
#
# All rights reserved - Do Not Redistribute
#
package_url = "http://pcsomewhere2.rim.net/packages/chefdk_0.3.5-1_amd64.deb"
package_name = ::File.basename(package_url)
package_local_path = "#{Chef::Config[:file_cache_path]}/#{package_name}"

remote_file package_local_path do
  source package_url
end

package package_local_path do
  provider Chef::Provider::Package::Dpkg
end