name             'dotfiles'
maintainer       'Aziz Ahmad Afandi'
maintainer_email 'aziz1967@gmail.com'
license          'All rights reserved'
description      'Installs/Configures dotfiles'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
