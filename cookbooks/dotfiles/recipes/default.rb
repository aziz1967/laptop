#
# Cookbook Name:: dotfiles
# Recipe:: default
#
# Copyright 2015, Aziz Ahmad Afandi
#
# All rights reserved - Do Not Redistribute
#

username = node['laptop']['username']

cookbook_file "/etc/bash.bashrc" do
  source 'bash_bashrc'
  owner "root"
  group "root"
  mode '0644'
end

cookbook_file "/home/#{username}/.screenrc" do
  source 'screenrc'
  owner "#{username}"
  group "#{username}"
  mode '0664'
end

cookbook_file "/home/#{username}/.vimrc" do
  source 'vimrc'
  owner "#{username}"
  group "#{username}"
  mode '0664'
end
