wallpaper CHANGELOG
===================

This file is used to list changes made in each version of the wallpaper cookbook.

0.1.0
-----
- Aziz Ahmad Afandi - Initial release of wallpaper

- - -
Check the [Markdown Syntax Guide](http://daringfireball.net/projects/markdown/syntax) for help with Markdown.

The [Github Flavored Markdown page](http://github.github.com/github-flavored-markdown/) describes the differences between markdown on github and standard markdown.
