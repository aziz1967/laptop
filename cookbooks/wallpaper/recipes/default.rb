#
# Cookbook Name:: wallpaper
# Recipe:: default
#
# Copyright 2015, Aziz Ahmad Afandi
#
# All rights reserved - Do Not Redistribute
#

username = node['laptop']['username']

remote_file "#{Chef::Config[:file_cache_path]}/blackberry.bmp" do
  source 'http://pcsomewhere2.rim.net/images/wallpaper.bmp'
  notifies :run, 'execute[set Desktop background]', :immediately
end

execute 'set Desktop background' do
  command "gsettings set org.gnome.desktop.background picture-uri file:///#{Chef::Config[:file_cache_path]}/blackberry.bmp"
  user "#{username}"
  action :nothing
end