#
# Cookbook Name:: sublime-text
# Recipe:: default
#
# Copyright 2015, Aziz Ahmad Afandi
#
# All rights reserved - Do Not Redistribute
#

username = node['laptop']['username']

package_url = "https://download.sublimetext.com/sublime-text_build-3103_amd64.deb"
package_name = ::File.basename(package_url)
package_local_path = "#{Chef::Config[:file_cache_path]}/#{package_name}"

remote_file package_local_path do
  source package_url
end

package package_local_path do
  provider Chef::Provider::Package::Dpkg
end

base_dir = "/home/#{username}/.config/"
%w[ sublime-text-3 sublime-text-3/Packages sublime-text-3/Packages/User ].each do |path|
  directory "#{base_dir}#{path}" do
    owner "#{username}"
    group "#{username}" 
    mode '0700'
  end
end

cookbook_file "/home/#{username}/.config/sublime-text-3/Packages/User/Preferences.sublime-settings" do
  source 'Preferences.sublime-settings'
  owner "#{username}"
  group "#{username}"
  mode '0664'
  action :create
end
