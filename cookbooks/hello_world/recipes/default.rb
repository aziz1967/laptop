#
# Cookbook Name:: hellp_world
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
template "#{ENV['HOME']}/hello_world.txt" do
  source 'hello_world.txt.erb'
  mode '0644'
end