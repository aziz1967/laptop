#
# Cookbook Name:: aziznator
# Recipe:: default
#
# Copyright 2015, Aziz Ahmad Afandi
#
# All rights reserved - Do Not Redistribute
#
%w(vim htop rdesktop shutter guake chromium-browser vlc git screen libc6:i386 gcc-multilib audacity filezilla ttf-mscorefonts-installer mplayer smplayer browser-plugin-vlc tree dropbox fuse ntfs-3g).each do |my_package|
  package my_package
end

#Creatie symlink to Oracle Java for Firefox
link '/etc/alternatives/mozilla-javaplugin.so' do
  to '/usr/lib/jvm/default-java/jre/lib/amd64/libnpjp2.so'
end
