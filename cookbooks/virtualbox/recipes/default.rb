#
# Cookbook Name:: virtualbox
# Recipe:: default
#
# Copyright 2015, Aziz Ahmad Afandi
#
# All rights reserved - Do Not Redistribute
#

ora_key_url = "https://www.virtualbox.org/download/oracle_vbox.asc"
ora_key_name = ::File.basename(ora_key_url)
ora_key_local_path = "#{Chef::Config[:file_cache_path]}/#{ora_key_name}"

cookbook_file '/etc/apt/sources.list.d/virtualbox.list' do
  source 'virtualbox.list'
  owner 'root'
  group 'root'
  mode '0644'
end

remote_file ora_key_local_path do
  source ora_key_url
  notifies :run, 'execute[install Oracle key]', :immediately
  notifies :run, 'execute[update apt cache]', :immediately
end

execute 'install Oracle key' do
  command "apt-key add #{Chef::Config[:file_cache_path]}/#{ora_key_name}"
  action :nothing
end

execute 'update apt cache' do
  command 'apt-get -qq update'
  action :nothing
end

package "virtualbox-5.0" do
  action :install
end