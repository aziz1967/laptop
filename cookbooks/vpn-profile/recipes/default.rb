#
# Cookbook Name:: vpn-profile
# Recipe:: default
#
# Copyright 2015, Aziz Ahmad Afandi
#
# All rights reserved - Do Not Redistribute
#

%w(AMERICAS-RIMNET-BB2FA.xml APAC-RIMNET-BB2FA.xml eap1-ap-rim-net.xml eap1-na-rim-net.xml emea-rimnet-pswd.xml americas-rimnet-pswd.xml apac-rimnet-pswd.xml eap1-eu-rim-net.xml EMEA-RIMNET-BB2FA.xml).each do |profile|
  cookbook_file "/opt/cisco/anyconnect/profile/#{profile}" do
    source profile
    owner 'root'
    group 'root'
    mode '0644'
  end
end