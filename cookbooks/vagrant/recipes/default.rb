#
# Cookbook Name:: vagrant
# Recipe:: default
#
# Copyright 2015, Aziz Ahmad Afandi
#
# All rights reserved - Do Not Redistribute
#
#package_url = "http://pcsomewhere2.rim.net/packages/vagrant_1.7.2_x86_64.deb"
package_url = "https://releases.hashicorp.com/vagrant/1.8.1/vagrant_1.8.1_x86_64.deb"
package_name = ::File.basename(package_url)
package_local_path = "#{Chef::Config[:file_cache_path]}/#{package_name}"

remote_file package_local_path do
  source package_url
end

package package_local_path do
  provider Chef::Provider::Package::Dpkg
end