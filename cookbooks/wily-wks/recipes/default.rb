#
# Cookbook Name:: wily-wks
# Recipe:: default
#
# Copyright 2015, Aziz Ahmad Afandi
#
# All rights reserved - Do Not Redistribute
#

username = node['laptop']['username']

package_url = "http://pcsomewhere2.rim.net/packages/Introscope9.1.1.1.tgz"
package_name = ::File.basename(package_url)
package_local_path = "#{Chef::Config[:file_cache_path]}/#{package_name}"

# Add the raring repo
cookbook_file '/etc/apt/sources.list.d/ia32-libs-raring.list' do
  source 'ia32-libs-raring.list'
  owner 'root'
  group 'root'
  mode '0644'
  notifies :run, 'execute[update apt cache]', :immediately
  not_if do FileTest.directory?("/usr/share/doc/ia32-libs") end
end

execute 'update apt cache' do
  command 'apt-get -qq update'
  action :nothing
end

package 'ia32-libs' do
  action :install
end

file '/etc/apt/sources.list.d/ia32-libs-raring.list' do
  action :delete
end

remote_file package_local_path do
  source package_url
end

execute 'deploy wily' do
  command "tar -zxvf #{package_local_path} -C /home/#{username}/"
  user "#{username}"
  not_if do FileTest.directory?("/home/#{username}/Introscope9.1.1.1") end
end