#
# Cookbook Name:: eclipse
# Recipe:: default
#
# Copyright 2015, Aziz Ahmad Afandi
#
# All rights reserved - Do Not Redistribute
#

username = node['laptop']['username']

package_url = "http://pcsomewhere2.rim.net/packages/eclipse.tgz"
package_name = ::File.basename(package_url)
package_local_path = "#{Chef::Config[:file_cache_path]}/#{package_name}"

remote_file package_local_path do
  source package_url
end

execute 'deploy wily' do
  command "tar -zxvf #{package_local_path} -C /home/#{username}/"
  user username
  not_if do FileTest.directory?("/home/#{username}/eclipse") end
end