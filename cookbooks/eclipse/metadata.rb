name             'eclipse'
maintainer       'Aziz Ahmad Afandi'
maintainer_email 'aafandi@blackberry.com'
license          'All rights reserved'
description      'Installs/Configures eclipse'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
