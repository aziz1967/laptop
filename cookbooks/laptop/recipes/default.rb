#
# Cookbook Name:: laptop
# Recipe:: default
#
# Copyright 2015, Aziz Ahmad Afandi
#
# All rights reserved - Do Not Redistribute
#

#include_recipe "wallpaper"
include_recipe "vagrant"
include_recipe "sublime-text"
include_recipe "virtualbox"
include_recipe "java"
include_recipe "aziznator"
#include_recipe "chef-dk"
include_recipe "dotfiles"
#include_recipe "wily-wks"
#include_recipe "eclipse"
#include_recipe "vpn-profile"
