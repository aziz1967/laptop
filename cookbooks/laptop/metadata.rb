name             'laptop'
maintainer       'Aziz Ahmad Afandi'
maintainer_email 'aziz1967@gmail.com'
license          'All rights reserved'
description      'Installs/Configures laptop'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

#depends "wallpaper"
depends "vagrant"
depends "sublime-text"
depends "virtualbox"
depends "aziznator"
depends "chef-dk"
depends "dotfiles"
depends "wily-wks"
depends "eclipse"
depends "vpn-profile"
depends "java"