Overview
========

This repo is used to provision a Ubuntu 14.04 desktop/laptop after the base OS has been installed.

To use it:
1.  Install Chef DK from Chef website. Do not use chef from the repo
2.  Modify the default attributes "username" in the laptop cookbook.  Change it to your own username
3.  Run the recipe[laptop]. 
4.  At the end of the run, you laptop would have been provisioned with the following

    - Virtualbox 5.0
    - Vagrant 1.8.1
    - Sublime-text 3 (with tab spaces changed to 2 from the default 4)
    - customised dotfile for screen, vim and /etc/bash.bashrc
    - Chef Development Kit 0.11.0
    - various Ubuntu packages. Modify the recipe at aziznator to your liking. Currently the following packages are pulled from the Ubuntu repository
      - vim htop rdesktop shutter guake chromium-browser vlc pidgin pidgin-sipe git screen libc6:i386 gcc-multilib audacity filezilla ttf-mscorefonts-installer mplayer smplayer browser-plugin-vlc tree dropbox fuse ntfs-3g
    - Oracle JDK 8

Usage (Using chef-solo)
=======================
$ sudo chef-solo -c solo.rb -j solo.json