root = File.absolute_path(File.dirname(__FILE__))

file_cache_path '/var/chef/cache'
cookbook_path root + '/cookbooks'

